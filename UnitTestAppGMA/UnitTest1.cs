﻿using System;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;
using System.IO;
using System.Text;
using System.Runtime.Serialization.Json;
using GolfLigueConsole;
using System.Collections.Generic;

namespace UnitTestAppGMA
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestJsonDecode()
        {
            string unJSON = "[{\"Code\":\"C0001\",\"Nom\":\"SLAM golf Club\",\"Rue\":\"rue de la prairie\",\"Cp\":\"55000\",\"Ville\":\"BAR LE DUC\",\"Gps_lat\":48.758224,\"Gps_lon\":5.115337,\"Tel\":\"0329845151\",\"Email\":\"siogolfclubslam@sio.fr\",\"Licencie\":14}]";

            var stream = new MemoryStream(Encoding.UTF8.GetBytes(unJSON));
            var jsonSerializer = new DataContractJsonSerializer(typeof(List<Club>));
            List<Club> lesGolfs = (List<Club>)jsonSerializer.ReadObject(stream);

            Assert.AreEqual(lesGolfs[0].Code, "C0001", "Problème de décodage JSON");
        }
    }
}
