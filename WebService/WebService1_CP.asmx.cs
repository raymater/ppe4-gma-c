﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using MySql.Data.MySqlClient.Properties;
using MySql.Data.Common;
using MySql.Data.Types;
using MySql.Data.MySqlClient;
using System.Web.DataAccess;
using System.Globalization;

namespace WebService
{
    /// <summary>
    /// Description résumée de WebService1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Pour autoriser l'appel de ce service Web depuis un script à l'aide d'ASP.NET AJAX, supprimez les marques de commentaire de la ligne suivante. 
    [System.Web.Script.Services.ScriptService]
    public class WebService1 : System.Web.Services.WebService
    {

        public MySqlConnection conn = new MySql.Data.MySqlClient.MySqlConnection();

        [WebMethod]
        public string recupTrous(string code)
        {
            string myConnectionString;
            myConnectionString = "server=172.18.207.102;uid=golfede;pwd=golfede;database=golfbdd;";
            conn.ConnectionString = myConnectionString;
            conn.Open();

            string requete = "SELECT * FROM trou WHERE code_club = '" + code + "'";
            MySqlCommand cmd = new MySqlCommand(requete, conn);
            MySqlDataReader rdr = cmd.ExecuteReader();

            string JSON = "[";
            int i = 1;

            while(rdr.Read())
            {
                if(i != 1)
                {
                    JSON = JSON + ",";
                }

                JSON = JSON + "{\"Numero\":" + rdr["numero"].ToString() + ",\"Distance\":" + rdr["distance"].ToString() + ",\"Par\":" + rdr["par"].ToString() + "}";

                i++;
            }

            rdr.Close();
            conn.Close();

            JSON = JSON + "]";

            return JSON;
        }

        [WebMethod]
        public string recupGolfs(string recherche)
        {
            string myConnectionString;
            myConnectionString = "server=172.18.207.102;uid=golfede;pwd=golfede;database=golfbdd;";
            conn.ConnectionString = myConnectionString;
            conn.Open();

            string requete = "SELECT * FROM club WHERE code LIKE '%" + recherche + "%' OR nom LIKE '%" + recherche + "%' OR rue LIKE '%" + recherche + "%' OR cp LIKE '%" + recherche + "%' OR ville = '%" + recherche + "%' OR tel LIKE '%" + recherche + "%' OR email LIKE '%" + recherche + "%'";
            MySqlCommand cmd = new MySqlCommand(requete, conn);
            MySqlDataReader rdr = cmd.ExecuteReader();

            string JSON = "[";
            int i = 1;

            while (rdr.Read())
            {
                if (i != 1)
                {
                    JSON = JSON + ",";
                }

                string lat = rdr["latitude"].ToString();
                string lon = rdr["longitude"].ToString();
                lat = lat.Replace(',', '.');
                lon = lon.Replace(',', '.');

                JSON = JSON + "{\"Code\":\"" + rdr["code"].ToString() + "\",\"Nom\":\"" + rdr["nom"].ToString() + "\",\"Rue\":\"" + rdr["rue"].ToString() + "\",\"Cp\":\"" + rdr["cp"].ToString() + "\",\"Ville\":\"" + rdr["ville"].ToString() + "\",\"Gps_lat\":" + lat.ToString(CultureInfo.InvariantCulture) + ",\"Gps_lon\":" + lon + ",\"Tel\":\"" + rdr["tel"].ToString() + "\",\"Email\":\"" + rdr["email"].ToString() + "\",\"Licencie\":" + rdr["licencies"].ToString() + "}";

                i++;
            }

            rdr.Close();
            conn.Close();

            JSON = JSON + "]";

            return JSON;
        }
    }
}
