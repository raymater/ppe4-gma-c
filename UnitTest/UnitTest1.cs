﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GolfLigueConsole;
using System.Net;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using System.Text;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Runtime.Serialization;

namespace UnitTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestJSONDecode()
        {
            string unJSON = "[{\"Code\":\"C0001\",\"Nom\":\"SLAM golf Club\",\"Rue\":\"rue de la prairie\",\"Cp\":\"55000\",\"Ville\":\"BAR LE DUC\",\"Gps_lat\":48.758224,\"Gps_lon\":5.115337,\"Tel\":\"0329845151\",\"Email\":\"siogolfclubslam@sio.fr\",\"Licencie\":14}]";

            var stream = new MemoryStream(Encoding.UTF8.GetBytes(unJSON));
            var jsonSerializer = new DataContractJsonSerializer(typeof(List<Club>));
            List<Club> lesGolfs = (List<Club>)jsonSerializer.ReadObject(stream);
        }
    }
}
