﻿using GolfLigueConsole;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOLF_GMA
{
    public class Golf
    {
        public string Code { get; set; }
        public string Nom { get; set; }
        public string Rue { get; set; }
        public string Cp { get; set; }
        public string Ville { get; set; }
        public decimal Gps_lat { get; set; }
        public decimal Gps_lon { get; set; }
        public string Tel { get; set; }
        public string Email { get; set; }
        public int Licencie { get; set; }
        //public List<Trou> lesTrous { get; set; }

        public Golf(string leCode, string leNom, string laRue, string leCp, string laVille, decimal leGps_lat, decimal leGps_long, string leTel, string lEmail, int leLicencie)
        {
            this.Code = leCode;
            this.Nom = leNom;
            this.Rue = laRue;
            this.Cp = leCp;
            this.Ville = laVille;
            this.Gps_lat = leGps_lat;
            this.Gps_lon = leGps_long;
            this.Tel = leTel;
            this.Email = lEmail;
            this.Licencie = leLicencie;
            //this.lesTrous = sesTrous;
        }

        public override string ToString()
        {
            return (this.Nom + "\n Adresse : " + this.Rue + "\n" + this.Cp + " " + this.Ville + "\n Téléphone : " + this.Tel + "\n Email : " + this.Email + "\n GPS : (" + this.Gps_lat.ToString() + ";" + this.Gps_lon.ToString() + "\n Nombre de licenciés : " + this.Licencie.ToString());
        }
    }
}
