﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Pour en savoir plus sur le modèle d’élément Page vierge, consultez la page http://go.microsoft.com/fwlink/?LinkID=390556

namespace GOLF_GMA
{
    /// <summary>
    /// Une page vide peut être utilisée seule ou constituer une page de destination au sein d'un frame.
    /// </summary>
    public sealed partial class ConnexionPage : Page
    {
        private const string JSONFILENAME = "connexion.json";

        public ConnexionPage()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Invoqué lorsque cette page est sur le point d'être affichée dans un frame.
        /// </summary>
        /// <param name="e">Données d'événement décrivant la manière dont l'utilisateur a accédé à cette page.
        /// Ce paramètre est généralement utilisé pour configurer la page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        // BOUTON Retour
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //MainPage.retourConnexion = false;
            this.Frame.GoBack();
        }

        // BOUTON Connexion (S'authentifier)
        private async void Button_Click_1(object sender, RoutedEventArgs e)
        {
            MessageDialog msgbox = null;

            if(login.Text != "" && mdp.Password != "")
            {
                // On tente l'authentification
                using (var httpClient = new HttpClient())
                {
                    // on spécifie l'adresse web de la ressource à atteindre et on prépare l'entête http en tant qu'appli json
                    httpClient.BaseAddress = new Uri("http://172.18.201.135/ppe4-gma-php/util/");
                    httpClient.DefaultRequestHeaders.Accept.Clear();
                    httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    //System.Diagnostics.Debug.WriteLine("LE JSON : " + sr.ReadToEnd().ToString());

                    // exemple montrant un envoi de données clés valeurs sous forme de texte
                    var pairs = new List<KeyValuePair<string, string>>
                    {
                        new KeyValuePair<string, string>("login", login.Text),
                        new KeyValuePair<string, string>("mdp", mdp.Password)
                    };
                    // on encode le tout au format url (clé=valeur&cle2=valeur2 ...)
                    var content = new FormUrlEncodedContent(pairs);

                    // formule l'appel au script distant en lui passant les données en POST
                    HttpResponseMessage response = httpClient.PostAsync("authentification.php", content).Result;

                    //HttpResponseMessage response = httpClient.PostAsync("ws_phone_test1.php", new StringContent(sr.ReadToEnd(), Encoding.UTF8, "application/json")).Result;

                    string statusCode = response.StatusCode.ToString();

                    try
                    {
                        response.EnsureSuccessStatusCode();
                        Task<string> responseBody = response.Content.ReadAsStringAsync();

                        var jsonSerializer = new DataContractJsonSerializer(typeof(Message));
                        var stream = new MemoryStream(Encoding.UTF8.GetBytes(responseBody.Result));
                        Message m = (Message)jsonSerializer.ReadObject(stream);

                        if (m.Code == "1")
                        {
                            // Ecriture dans le JSON des identifiants
                            List<string> identifiants = new List<string>();
                            identifiants.Clear();
                            MainPage.lesIdentifiants.Clear();
                            identifiants.Add(login.Text);
                            identifiants.Add(mdp.Password);
                            MainPage.lesIdentifiants = identifiants;

                            var serializer = new DataContractJsonSerializer(typeof(List<string>));
                            using (var stream2 = await ApplicationData.Current.LocalFolder.OpenStreamForWriteAsync(
                                            JSONFILENAME,
                                            CreationCollisionOption.ReplaceExisting))
                            {
                                serializer.WriteObject(stream2, identifiants);
                            }

                            
                            this.Frame.GoBack();
                        }
                        else
                        {
                            msgbox = new MessageDialog("Login/Mot de passe incorrect(s) !", "Erreur de connexion");
                        }
                    }
                    catch
                    {
                        msgbox = new MessageDialog("Connexion impossible avec le serveur", "Erreur de connexion");
                    }

                    if (msgbox != null)
                    {
                        await msgbox.ShowAsync();
                    }
                    else
                    {
                        MainPage.retourConnexion = true;
                    }
                }
            }
            else
            {
                // Un des champs sont vides = ERREUR
                msgbox = new MessageDialog("Champs login/mot de passe obligatoires", "Champs non-remplis");
                await msgbox.ShowAsync();
            }
        }


    }
}
