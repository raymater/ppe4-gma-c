﻿using GolfLigueConsole;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Net.Http;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Runtime.Serialization.Json;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Popups;
using System.Net.Http.Headers;
using System.Xml.Linq;

// Pour en savoir plus sur le modèle d'élément Page vierge, consultez la page http://go.microsoft.com/fwlink/?LinkId=391641

namespace GOLF_GMA
{
    /// <summary>
    /// Une page vide peut être utilisée seule ou constituer une page de destination au sein d'un frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public static string JSON_Golf = "[{\"Code\":\"C0001\",\"Nom\":\"SLAM golf Club\",\"Rue\":\"rue de la prairie\",\"Cp\":\"55000\",\"Ville\":\"BAR LE DUC\",\"Gps_lat\":48.758224,\"Gps_lon\":5.115337,\"Tel\":\"0329845151\",\"Email\":\"siogolfclubslam@sio.fr\",\"Licencie\":14},{\"Code\":\"C0002\",\"Nom\":\"SISR golf Club\",\"Rue\":\"rue de la prairie\",\"Cp\":\"55000\",\"Ville\":\"BAR LE DUC\",\"Gps_lat\":48.758224,\"Gps_lon\":5.115337,\"Tel\":\"0329845151\",\"Email\":\"siogolfclubsisr@sio.fr\",\"Licencie\":12},{\"Code\":\"C0003\",\"Nom\":\"SIO1 golfClub\",\"Rue\":\"rue de la prairie\",\"Cp\":\"55000\",\"Ville\":\"BAR LE DUC\",\"Gps_lat\":48.758224,\"Gps_lon\":5.115337,\"Tel\":\"0329845151\",\"Email\":\"siogolfclubsio1@sio.fr\",\"Licencie\":33}]";

        public static List<Club> lesGolfs = null;

        public static Club ClubSelect = null;

        public static bool retourConnexion = false;

        public static List<string> lesIdentifiants = new List<string>();

        public static string JSON_Score = "score.json";

        public static string JSON_Parcours = "parcours.json";

        public MainPage()
        {
            this.InitializeComponent();

            this.NavigationCacheMode = NavigationCacheMode.Required;

            charger();
            //await verifierconnexion();

            
        }

        public async void charger()
        {
            await verifierconnexion();
        }

        /// <summary>
        /// Invoqué lorsque cette page est sur le point d'être affichée dans un frame.
        /// </summary>
        /// <param name="e">Données d’événement décrivant la manière dont l’utilisateur a accédé à cette page.
        /// Ce paramètre est généralement utilisé pour configurer la page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            // TODO: préparer la page pour affichage ici.

            // TODO: si votre application comporte plusieurs pages, assurez-vous que vous
            // gérez le bouton Retour physique en vous inscrivant à l’événement
            // Windows.Phone.UI.Input.HardwareButtons.BackPressed.
            // Si vous utilisez le NavigationHelper fourni par certains modèles,
            // cet événement est géré automatiquement.

            listgolf.SelectedIndex = -1;
            messageText.Text = "-";
        }

        // BOUTON Synchroniser
        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            MessageDialog msgbox = null;

            if(retourConnexion == true)
            {
                // $_POST["login"] : dans le JSON de connexion :
                Stream myStream = null;
                StorageFile file = null;
                file = await ApplicationData.Current.LocalFolder.GetFileAsync("connexion.json");
                myStream = await file.OpenStreamForReadAsync();
                myStream.Dispose();
                List<string> MesIds;
                var jsonSerializer = new DataContractJsonSerializer(typeof(List<string>));
                myStream = await ApplicationData.Current.LocalFolder.OpenStreamForReadAsync("connexion.json");
                MesIds = (List<string>)jsonSerializer.ReadObject(myStream);
                string login = MesIds[0];

                String score = "";
                String club = "";

                try
                {
                    // $_POST["score"] : dans le JSON de scores :
                    file = await ApplicationData.Current.LocalFolder.GetFileAsync("score.json");
                    myStream = await file.OpenStreamForReadAsync();
                    myStream.Dispose();
                    List<int> MesScores;
                    var jsonSerializer2 = new DataContractJsonSerializer(typeof(List<int>));
                    myStream = await ApplicationData.Current.LocalFolder.OpenStreamForReadAsync("score.json");
                    MesScores = (List<int>)jsonSerializer2.ReadObject(myStream);
                    var serializer = new DataContractJsonSerializer(typeof(List<int>));
                    var stream_post = new MemoryStream();
                    serializer.WriteObject(stream_post, MesScores);
                    stream_post.Position = 0;
                    StreamReader sr = new StreamReader(stream_post);
                    //System.Diagnostics.Debug.WriteLine("DEBUG contenuStream : " + sr.ReadToEnd());
                    score = sr.ReadToEnd();

                    // $_POST["club"] : dans le JSON de parcours :
                    file = await ApplicationData.Current.LocalFolder.GetFileAsync("parcours.json");
                    myStream = await file.OpenStreamForReadAsync();
                    myStream.Dispose();
                    var jsonSerializer3 = new DataContractJsonSerializer(typeof(string));
                    myStream = await ApplicationData.Current.LocalFolder.OpenStreamForReadAsync("parcours.json");
                    club = (string)jsonSerializer3.ReadObject(myStream);
                }
                catch
                {
                    msgbox = new MessageDialog("Pas de scores/parcours à traiter", "Erreur");
                }

                if (msgbox == null)
                {
                    // Envoi des infos au script PHP
                    using (var httpClient = new HttpClient())
                    {
                        // on spécifie l'adresse web de la ressource à atteindre et on prépare l'entête http en tant qu'appli json
                        httpClient.BaseAddress = new Uri("http://172.18.201.135/ppe4-gma-php/util/");
                        httpClient.DefaultRequestHeaders.Accept.Clear();
                        httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                        //System.Diagnostics.Debug.WriteLine("LE JSON : " + sr.ReadToEnd().ToString());

                        // exemple montrant un envoi de données clés valeurs sous forme de texte
                        var pairs = new List<KeyValuePair<string, string>>
                        {
                            new KeyValuePair<string, string>("login", login),
                            new KeyValuePair<string, string>("score", score),
                            new KeyValuePair<string, string>("club", club)
                        };
                        // on encode le tout au format url (clé=valeur&cle2=valeur2 ...)
                        var content = new FormUrlEncodedContent(pairs);

                        // formule l'appel au script distant en lui passant les données en POST
                        HttpResponseMessage response = httpClient.PostAsync("envoi.php", content).Result;

                        //HttpResponseMessage response = httpClient.PostAsync("ws_phone_test1.php", new StringContent(sr.ReadToEnd(), Encoding.UTF8, "application/json")).Result;

                        string statusCode = response.StatusCode.ToString();

                        try
                        {
                            response.EnsureSuccessStatusCode();
                            Task<string> responseBody = response.Content.ReadAsStringAsync();

                            var jsonSerializer5 = new DataContractJsonSerializer(typeof(Message));
                            var stream = new MemoryStream(Encoding.UTF8.GetBytes(responseBody.Result));
                            Message m = (Message)jsonSerializer5.ReadObject(stream);

                            if (m.Code == "1")
                            {
                                messageText.Text = m.Libelle;

                                // Effacer le contenu local
                                file = await ApplicationData.Current.LocalFolder.GetFileAsync("parcours.json");
                                await file.DeleteAsync();

                                file = await ApplicationData.Current.LocalFolder.GetFileAsync("score.json");
                                await file.DeleteAsync();

                                CartePage.score.Clear();
                            }
                            else
                            {
                                msgbox = new MessageDialog(m.Libelle);
                            }
                        }
                        catch
                        {
                            msgbox = new MessageDialog("Connexion impossible avec le serveur", "Erreur de connexion");
                        }

                    }
                }

                if(msgbox != null)
                {
                    await msgbox.ShowAsync();
                }
            }
        }

        // BOUTON Connexion
        private async void Button_Click_1(object sender, RoutedEventArgs e)
        {
            if (connexion.Content.ToString() == "Connexion")
            {
                this.Frame.Navigate(typeof(ConnexionPage));
                
            }
            else
            {
                StorageFile filed = await ApplicationData.Current.LocalFolder.GetFileAsync("connexion.json");
                if (filed != null)
                {
                    await filed.DeleteAsync();
                }
                lesIdentifiants.Clear();

                await verifierconnexion();

                messageText.Text = "Déconnecté !";
            }
        }

        // BOUTON OK : Recherche
        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            // recherche.Text

            var httpClient = new System.Net.Http.HttpClient();
            httpClient.DefaultRequestHeaders.Accept.Add(new
            MediaTypeWithQualityHeaderValue("text/xml"));
            httpClient.DefaultRequestHeaders.Add("SOAPAction", "http://tempuri.org/recupGolfs");
            var soapXml = "<?xml version=\"1.0\" encoding=\"utf-8\"?><soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\"><soap12:Body><recupGolfs xmlns=\"http://tempuri.org/\"><recherche>" + recherche.Text + "</recherche></recupGolfs></soap12:Body></soap12:Envelope>";
            var response = httpClient.PostAsync("http://172.18.207.204/WebService1_CP.asmx", new StringContent(soapXml, Encoding.UTF8, "text/xml")).Result;
            var content = response.Content.ReadAsStringAsync().Result;
            XDocument responseXML = XDocument.Parse(content);

            System.Diagnostics.Debug.WriteLine("DEBUG MESSAGE - JSON : " + responseXML.Root.Value);

            // Générer le JSON du Golf en classe (objet)

            var stream = new MemoryStream(Encoding.UTF8.GetBytes(responseXML.Root.Value));
            var jsonSerializer = new DataContractJsonSerializer(typeof(List<Club>));
            lesGolfs = (List<Club>)jsonSerializer.ReadObject(stream);

            listgolf.Items.Clear();

            foreach(Club unClub in lesGolfs)
            {
                listgolf.Items.Add(unClub);
            }
            
        }

        public async Task verifierconnexion()
        {
            try
            {
                // Déserialiser les identifiants
                await deserializeJsonAsync();

                retourConnexion = true;
                connexion.Content = "Se déconnecter";
                syncButton.Visibility = Visibility.Visible;
            }
            catch
            {
                retourConnexion = false;
                connexion.Content = "Connexion";
                syncButton.Visibility = Visibility.Collapsed;
            }
        }

        private async Task deserializeJsonAsync()
        {
            //bool fileExists = true;
            Stream myStream = null;
            StorageFile file = null;

            file = await ApplicationData.Current.LocalFolder.GetFileAsync("connexion.json");
            myStream = await file.OpenStreamForReadAsync();
            myStream.Dispose();

            List<string> MesIds;

            var jsonSerializer = new DataContractJsonSerializer(typeof(List<string>));

            myStream = await ApplicationData.Current.LocalFolder.OpenStreamForReadAsync("connexion.json");

            MesIds = (List<string>)jsonSerializer.ReadObject(myStream);

            lesIdentifiants.Clear();
            lesIdentifiants = MesIds;
        }

        private async void select_item(object sender, SelectionChangedEventArgs e)
        {
            if(listgolf.SelectedIndex != -1)
            {
                ClubSelect = (Club)listgolf.Items[listgolf.SelectedIndex];
                if(ClubSelect != CartePage.ancienClubSelect)
                {
                    if (CartePage.ancienClubSelect != null)
                    {
                        MessageDialog msg = new MessageDialog("Vous êtes sur le point de changer de club, les scores obtenus jusqu'ici seront effacés pour laisser place aux score du club sélectionné ! Souhaitez-vous poursuivre malgré tout ?", "Avertissement");
                        msg.Commands.Add(new UICommand("Continuer", new UICommandInvokedHandler(CommandHandler)));
                        msg.Commands.Add(new UICommand("Annuler", new UICommandInvokedHandler(CommandHandler)));
                        await msg.ShowAsync();
                    }
                    else
                    {
                        this.Frame.Navigate(typeof(TrousPage));
                    }
                }
                else
                {
                    this.Frame.Navigate(typeof(TrousPage));
                }
                
            }
            //System.Diagnostics.Debug.WriteLine("******** ---------- Oui !!!!!!!!!!!!!!!!   " + listgolf.SelectedIndex.ToString());
        }

        private async void voirPage(object sender, RoutedEventArgs e)
        {
            //System.Diagnostics.Debug.WriteLine("******** ---------- Oui !!!!!!!!!!!!!!!!");

            await verifierconnexion();
        }

        private async void CommandHandler(IUICommand command)
        {
            switch(command.Label)
            {
                case "Continuer":
                    this.Frame.Navigate(typeof(TrousPage));

                    // Effacer JSON local
                    var file = await ApplicationData.Current.LocalFolder.GetFileAsync("parcours.json");
                    await file.DeleteAsync();

                    file = await ApplicationData.Current.LocalFolder.GetFileAsync("score.json");
                    await file.DeleteAsync();

                    break;
                default:
                    break;
            }
        }
    }
}
