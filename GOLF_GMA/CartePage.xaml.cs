﻿using GolfLigueConsole;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Popups;
using System.Runtime.Serialization.Json;
using Windows.Storage;

// Pour en savoir plus sur le modèle d’élément Page vierge, consultez la page http://go.microsoft.com/fwlink/?LinkID=390556

namespace GOLF_GMA
{
    /// <summary>
    /// Une page vide peut être utilisée seule ou constituer une page de destination au sein d'un frame.
    /// </summary>
    public sealed partial class CartePage : Page
    {
        public static List<int> score = new List<int>();

        public static Club ancienClubSelect = null;

        public CartePage()
        {
            this.InitializeComponent();

            if (score.Count() != TrousPage.lesTrous.Count())
            {
                score.AddRange(Enumerable.Repeat(0, TrousPage.lesTrous.Count()));
            }
        }

        /// <summary>
        /// Invoqué lorsque cette page est sur le point d'être affichée dans un frame.
        /// </summary>
        /// <param name="e">Données d'événement décrivant la manière dont l'utilisateur a accédé à cette page.
        /// Ce paramètre est généralement utilisé pour configurer la page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (ancienClubSelect != MainPage.ClubSelect && TrousPage.chargerJSON == false)
            {
                // Réinitialiser tout quand c'est un autre club qui est sélectionné !
                score.Clear();
                score.AddRange(Enumerable.Repeat(0, TrousPage.lesTrous.Count()));
            }
            
            if(TrousPage.chargerJSON == true)
            {
                TrousPage.chargerJSON = false;
            }

            ancienClubSelect = MainPage.ClubSelect;

            grilleScore.Items.Clear();

            foreach(Trou leTrou in TrousPage.lesTrous)
            {
                grilleScore.Items.Add(leTrou);
            }

            grilleScore.SelectedIndex = 0;
        }

        private async void retourBouton_Click(object sender, RoutedEventArgs e)
        {
            // Encoder le parcours du score en JSON
            var serializer2 = new DataContractJsonSerializer(typeof(string));
            using (var stream3 = await ApplicationData.Current.LocalFolder.OpenStreamForWriteAsync(
                            MainPage.JSON_Parcours,
                            CreationCollisionOption.ReplaceExisting))
            {
                serializer2.WriteObject(stream3, MainPage.ClubSelect.Code);
            }

            // Encoder le score en JSON
            var serializer = new DataContractJsonSerializer(typeof(List<int>));
            using (var stream2 = await ApplicationData.Current.LocalFolder.OpenStreamForWriteAsync(
                            MainPage.JSON_Score,
                            CreationCollisionOption.ReplaceExisting))
            {
                serializer.WriteObject(stream2, score);
            }

            this.Frame.Navigate(typeof(MainPage));
        }

        private void changerTrou(object sender, SelectionChangedEventArgs e)
        {
            Trou leTrou = (Trou)grilleScore.SelectedItem;
            parText.Text = leTrou.Par.ToString();

            if(grilleScore.SelectedIndex == TrousPage.lesTrous.Count() - 1)
            {
                suivantBouton.Content = "Terminer la partie";
            }
            else
            {
                suivantBouton.Content = "Trou suivant >>";
            }

            if(score[grilleScore.SelectedIndex] != 0)
            {
                scoreBox.Text = score[grilleScore.SelectedIndex].ToString();
            }
        }

        private async void suivantBouton_Click(object sender, RoutedEventArgs e)
        {
            int number;
            bool result = Int32.TryParse(scoreBox.Text, out number);
            //int count = TrousPage.lesTrous.Count();

            if (scoreBox.Text != null && scoreBox.Text != "0" && result == true)
            {
                score[grilleScore.SelectedIndex] = Convert.ToInt16(scoreBox.Text.ToString());

                if ((grilleScore.SelectedIndex == (TrousPage.lesTrous.Count() - 1)) && (score.Contains(0) == false))
                {
                    // Dernier trou rempli : fin de la partie !

                    // Calcul du score :
                    int total = 0;
                    for (int i = 0; i < score.Count(); i++)
                    {
                        total = total + score[i];
                    }

                    // Par total :
                    int parTotal = 0;
                    for (int i = 0; i < TrousPage.lesTrous.Count(); i++)
                    {
                        parTotal = parTotal + TrousPage.lesTrous[i].Par;
                    }

                    // +/- Par :
                    string difference;
                    if (total - parTotal > 0)
                    {
                        difference = "+" + Convert.ToString(total - parTotal);
                    }
                    else
                    {
                        difference = Convert.ToString(total - parTotal);
                    }

                    MessageDialog msgbox = new MessageDialog("Nombre de coups : " + total.ToString() + "\nPar total : " + parTotal.ToString() + "\nVotre score : " + difference, "Fin de la partie !");
                    await msgbox.ShowAsync();
                }
                else
                {
                    // Passer au trou suivant

                    grilleScore.SelectedIndex = grilleScore.SelectedIndex + 1;
                    if (score[grilleScore.SelectedIndex] != 0)
                    {
                        scoreBox.Text = score[grilleScore.SelectedIndex].ToString();
                    }
                    else
                    {
                        scoreBox.Text = "";
                    }
                }
            }
            else
            {
                MessageDialog msgbox = new MessageDialog("Score incorrect !", "Erreur de saisie");
                await msgbox.ShowAsync();
            }
        }
    }
}
