﻿using GolfLigueConsole;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Xml.Linq;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.Web.Http;

// Pour en savoir plus sur le modèle d’élément Page vierge, consultez la page http://go.microsoft.com/fwlink/?LinkID=390556

namespace GOLF_GMA
{
    /// <summary>
    /// Une page vide peut être utilisée seule ou constituer une page de destination au sein d'un frame.
    /// </summary>
    public sealed partial class TrousPage : Page
    {
        public static List<Trou> lesTrous = null;

        public static bool chargerJSON = false;

        public TrousPage()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Invoqué lorsque cette page est sur le point d'être affichée dans un frame.
        /// </summary>
        /// <param name="e">Données d'événement décrivant la manière dont l'utilisateur a accédé à cette page.
        /// Ce paramètre est généralement utilisé pour configurer la page.</param>
        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            infosBox.Text = MainPage.ClubSelect.ToString();

            trousList.Items.Clear();

            // un Webservice renverra l'ensemble des trous d'un parcours selon le club demandé :
            var httpClient = new System.Net.Http.HttpClient();
            httpClient.DefaultRequestHeaders.Accept.Add(new
            MediaTypeWithQualityHeaderValue("text/xml"));
            httpClient.DefaultRequestHeaders.Add("SOAPAction", "http://tempuri.org/recupTrous");
            var soapXml = "<?xml version=\"1.0\" encoding=\"utf-8\"?><soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\"><soap12:Body><recupTrous xmlns=\"http://tempuri.org/\"><code>" + MainPage.ClubSelect.Code + "</code></recupTrous></soap12:Body></soap12:Envelope>";
            var response = httpClient.PostAsync("http://172.18.207.204/WebService1_CP.asmx", new StringContent(soapXml, Encoding.UTF8, "text/xml")).Result;
            var content = response.Content.ReadAsStringAsync().Result;
            XDocument responseXML = XDocument.Parse(content);

            System.Diagnostics.Debug.WriteLine("DEBUG MESSAGE - JSON : " + responseXML.Root.Value);

            var stream = new MemoryStream(Encoding.UTF8.GetBytes(responseXML.Root.Value));
            var jsonSerializer = new DataContractJsonSerializer(typeof(List<Trou>));
            lesTrous = (List<Trou>)jsonSerializer.ReadObject(stream);

            foreach(Trou unTrou in lesTrous)
            {
                trousList.Items.Add(unTrou);
            }




            // Vérifier si les scores du parcours enregistré correspondent bien au parcours sélectionné
            Stream myStream = null;
            StorageFile file = null;

            try
            {
                file = await ApplicationData.Current.LocalFolder.GetFileAsync("parcours.json");
                myStream = await file.OpenStreamForReadAsync();
                myStream.Dispose();

                string leParcours;

                var jsonSerializer2 = new DataContractJsonSerializer(typeof(string));

                myStream = await ApplicationData.Current.LocalFolder.OpenStreamForReadAsync("parcours.json");

                leParcours = (string)jsonSerializer2.ReadObject(myStream);

                if (MainPage.ClubSelect.Code == leParcours)
                {
                    chargerJSON = true;

                    file = await ApplicationData.Current.LocalFolder.GetFileAsync("score.json");
                    myStream = await file.OpenStreamForReadAsync();
                    myStream.Dispose();

                    List<int> MesScores;

                    var jsonSerializer3 = new DataContractJsonSerializer(typeof(List<int>));

                    myStream = await ApplicationData.Current.LocalFolder.OpenStreamForReadAsync("score.json");

                    MesScores = (List<int>)jsonSerializer3.ReadObject(myStream);

                    CartePage.score.Clear();
                    CartePage.score = MesScores;
                }
            }
            catch
            { }
        }

        // Bouton RETOUR
        private void backTrou_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.GoBack();
        }

        private void jouerButton_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(CartePage));
        }
    }
}
